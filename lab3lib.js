function addClass(obj, name) {
  if(obj.className.includes(name)) {
	return;
  } 
  if(obj.className.length != 0) {
	obj.className += ' ';
  }
  obj.className += name;
}

function camelize(str) {
  var newStr = "";
  var isDefise = false;
  
  for(var i = 0; i < str.length; i++) {
	  if(str.charAt(i) != '-') {
		if(isDefise) {
		  newStr += str.charAt(i).toUpperCase();
		  isDefise= false;
		} else {
		  newStr += str.charAt(i);
		}
	  } else {
		isDefise = true;
	  }
  }
  
  return newStr;
}

function removeClass(obj, name) {
	if(!obj.className.includes(name)) {
	  return;
	}
	
	var is = false;
	var newClassName = "";
	
	for(var i = 0; i < obj.className.length; i++) {
		is = true;
		for(var j = 0; j < name.length; j++) {
			if(obj.className.charAt(i + j) != name.charAt(j)) {
			  is = false;
			  break;
			}
		}
		
		
		if(is) {
		   newClassName = newClassName.substring(0, newClassName.length - 1);
		   i += name.length;
		}
		
		newClassName += obj.className.charAt(i);  
	}
	
	obj.className = newClassName.trim();
}

function filterRangeInPlace(arr, a, b) {

	for (var i = 0; i < arr.length; i++) {
		var val = arr[i];
		if (val < a || val > b) {
			arr.splice(i--, 1);
		}
	}
}

function reverseSort(arr) {
	arr.sort(function(a, b) {
		return b - a;
	});
}

function arraySort(arr) {
	return arr.sort(function(a, b) {
		return a.localeCompare(b);
	})
}

function sortObjByAge(arr) {
	return arr.sort(function(a, b) {
		return a.age - b.age;
	})
}

//10
function unique(arr) {
	var newArr = [];
	
	for(var i = 0; i < arr.length; i++) {
		var val = newArr.find(function(el) {
			return el == arr[i];
		});
		
		if(!val) {
			newArr.push(arr[i]);
		}
	}
	
	return newArr;
}

//9
var list = {
	value: 1,
	next: {
		value: 2,
		next: {
			value: 3,
			next: {
				value: 4,
				next: null
			}
		}
	}
};

function printList(list) {
	var end = true;
	var self = list;
	
	while (end) {
		console.log(self.value);
		self = self.next;
		
		if(!self) {
			end = false;
		}
	}
}

function printListRec(list) {
	if(!list) {
		return;
	}
	
	console.log(list.value);
	return printListRec(list.next);
}

function printListRecReverse(list, position) {
	var start = list;
	
	if(!position) {
		position = 1;
		
		while(start.next) {
			start = start.next;
			position++;
		}
	}
	
	start = list;
	
	for(var i = 0; i < position - 1; i++) {
		start = start.next;
	}
	
	console.log(start.value);
	
	if(position == 1) {
		return;
	}
	
	return printListReverse(list, --position);
}

function printListReverse(list) {
	var start = list;
	
	var position = 1;
	
	while(start.next) {
		start = start.next;
		position++;
	}

	
	while(position != 0) {
		start = list;
		for(var i = 0; i < position - 1; i++) {
			start = start.next;
		}
		
		console.log(start.value);
		position--;
	}
}
